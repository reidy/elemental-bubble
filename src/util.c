/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#include "conf.h"


/* ============================ C O L O R S   U T I L S ============================ */

/* Set the given color channels as main color */
void set_color (GdkGC * gc, int r, int g, int b)
{
    GdkColor c;

    c.pixel = 0;
    c.red = r << 8;
    c.green = g << 8;
    c.blue = b << 8;
    gdk_gc_set_rgb_fg_color (gc, &c);
}

/* Put an hexa color into three color channels */
void change_color (GdkGC * gc, int hexcode)
{
    int color[3];

    color [0] = (hexcode & 0xFF0000) >> 16;
    color [1] = (hexcode & 0x00FF00) >> 8;
    color [2] = (hexcode & 0x0000FF);

    set_color(gc, color [0], color [1], color [2]);
}


/* ================================== F R A M E S ================================== */

GtkWidget * frame_new (gchar * label, gboolean shadowed)
{
    GtkWidget *frame = gtk_frame_new (label);
    GtkShadowType type = shadowed ? GTK_SHADOW_ETCHED_IN : GTK_SHADOW_NONE;
    gtk_frame_set_shadow_type (GTK_FRAME (frame), type);
    return frame;
}


/* =============================== S T A T U S   B A R ============================= */

void statusbar_put (GtkStatusbar * bar, char * text)
{
    gtk_statusbar_pop (bar, 0);
    gtk_statusbar_push (bar, 0, text);
}


/* ============================== A R E A   R E D R A W ============================ */

void area_redraw (GtkWidget * area)
{
    gdk_window_invalidate_rect (area->window, NULL, FALSE);
}


/* =============================== I M A G E   D R A W ============================= */

void pixbuf_get_size (GdkPixbuf * pix, int * w, int * h)
{
    * w = gdk_pixbuf_get_width (pix);
    * h = gdk_pixbuf_get_height (pix);
}

GdkPixbuf * pixbuf_load (const char * filename)
{
    GError * error;
    GdkPixbuf * pix;
    int w, h;

    error = NULL;

    printf ("Loading %s ...", filename);
    fflush (stdout);
    pix = gdk_pixbuf_new_from_file (filename, & error);

    if (pix == NULL)
    {
        perror ("Unable to read file : %s on pixbuf_load.");
        g_error_free (error);

        return NULL;
    }

    pixbuf_get_size (pix, & w, & h);
    printf ("Size: (%d, %d)\n", w, h);

    return pix;
}

void pixbuf_draw (GdkWindow * win, GdkGC * gc, GdkPixbuf * pix, int x, int y)
{
    gdk_draw_pixbuf (win, gc, pix, 0, 0, x, y, -1, -1, GDK_RGB_DITHER_NONE, 0, 0);
}


/* ================================ B A L L   P O S =============================== */

int ball_pos_column_from_x (Level * level, Ball ball)
{
    int r;
    int full_ball = BALL_GAP + BALL_SIZE;

    if (ball_pos_line_from_y (level, ball) % 2)
    {
        r = (ball.x - BALL_GAP - BALL_SIZE / 2) / full_ball;
        return (r == NB_COLUMN - 1) ? NB_COLUMN - 2 : r;
    }
    return (ball.x - BALL_GAP) / full_ball;
}

int ball_pos_line_from_y (Level * level, Ball ball)
{
    return (ball.y - BALL_GAP - level->elevator * (BALL_GAP + BALL_SIZE) * 0.8) / ((BALL_GAP + BALL_SIZE) * 0.8);
}
