/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#include "conf.h"


/* ============================ C L I C K   E V E N T ============================= */

void on_start_clicked (GtkButton U * widget, gpointer U data)
{
    Gui * gui;
    Layout * l;
    Info * info;

    gui = gui_check (data);
    l = & gui->layout;
    info = gui->info;

    info->anim = !info->anim;
    if (info->anim)
    {
        gtk_button_set_label (GTK_BUTTON (l->startbutton), "Stop");
        info_anim_start (info, data);
    }
    else
    {
        gtk_button_set_label (GTK_BUTTON (l->startbutton), "Start");
        info_anim_stop (info);
    }
    state_show (info, gui->statusbar);
}


void on_reset_clicked (GtkButton U * widget, gpointer U data)
{
    Gui * gui;
    Info * info;

    gui = gui_check (data);
    info = gui->info;

    info->level_number = 0;
    next_level_init (info);

    state_show (info, gui->statusbar);
    gtk_widget_grab_focus (gui->layout.area);
}

/* ============================= K E Y   E V E N T ================================ */

gboolean on_area_key_espace_press (GtkWidget U * w, gpointer U data)
{
    Gui * gui;
    Info * info;
    gui = gui_check (data);
    info = gui->info;

    switch (info->state)
    {
    case S_BALL_IN_CANNON :
        info->state = S_BALL_LAUNCHED;
        info_anim_start (info, data);
        break;
    case S_LOST :
        info->level_number = 0;
        next_level_init (info);
        break;
    case S_WIN :
        info->level_number++;
        next_level_init (info);
        break;
    case S_GAME_DONE :
        info_anim_stop (info);
        break;
    default :
        break;
    }

    return TRUE;
}

gboolean on_area_key_press (GtkWidget U * w, GdkEventKey * e, gpointer U data)
{
    Gui * gui;
    Layout * l;
    Info * info;

    gui = gui_check (data);
    l = & gui->layout;
    info = gui->info;

    switch (e->keyval)
    {
    case GDK_Escape :
        gtk_button_set_label (GTK_BUTTON (l->startbutton), "Start");
        info_anim_stop (info);
        break;
    case GDK_space :
        on_area_key_espace_press (w, data) ;
        break;
    case GDK_Right :
        info->cannon_turn_right = 1;
        break;
    case GDK_Left :
        info->cannon_turn_left = 1;
        break;
    default :
        break;
    }

    return TRUE;
}

gboolean on_area_key_release (GtkWidget U * w, GdkEventKey U * e, gpointer U data)
{
    Gui * gui;
    Info * info;

    gui = gui_check (data);
    info = gui->info;

    switch (e->keyval)
    {
    case GDK_Right :
        info->cannon_turn_right = 0;
        break;
    case GDK_Left :
        info->cannon_turn_left = 0;
        break;
    default :
        break;
    }

    return TRUE;
}
