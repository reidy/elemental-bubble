/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#include "conf.h"


/* ========================== D R A W I N G   A R E A ============================= */

gboolean on_area_expose (GtkWidget * widget, GdkEventExpose U * e, gpointer U data)
{
    Gui * gui;
    GdkGC * gc;

    gui = gui_check (data);
    gc = gdk_gc_new (widget->window);

    pixbuf_draw (widget->window, gc, gui->image.background, 0, 0);
    level_draw (widget->window, gc, gui);
    cannon_ball_draw (widget->window, gc, gui);
    cannon_draw (widget->window, gui);
    cannon_next_ball_draw (widget->window, gc, gui->info);
    cannon_base_draw (widget->window, gc, gui);
    falling_ball_draw (widget->window, gc, gui);
    elevator_draw (widget->window, gc, gui);
    foreground_draw (widget, gc, gui);

    g_object_unref (gc);
    return TRUE;
}

void level_draw (GdkWindow * win, GdkGC * gc, Gui * gui)
{
    Info * info;
    Level level;
    Ball ball;
    int line, column;

    info = gui->info;
    level = info->level;

    for (line = 0; line < NB_LINE; line++)
    {
        for (column = 0; column < NB_COLUMN; column++)
        {
            ball = level.balls[column][line];

            if (ball.color != EMPTY)
            {
                ball_draw_from_pixbuf (win, gc, gui, ball);
            }
        }
    }
}

/* Display a Pixbuf from a given ball */
void ball_draw_from_pixbuf (GdkWindow * win, GdkGC * gc, Gui * gui, Ball ball)
{
    switch (ball.color) 
    {
    case EARTH:
        pixbuf_draw (win, gc, gui->image.b_earth, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case FIRE:
        pixbuf_draw (win, gc, gui->image.b_fire, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case WIND:
        pixbuf_draw (win, gc, gui->image.b_wind, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case WATER:
        pixbuf_draw (win, gc, gui->image.b_water, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case LIFE:
        pixbuf_draw (win, gc, gui->image.b_life, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case DARKNESS:
        pixbuf_draw (win, gc, gui->image.b_darkness, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case LIGHT:
        pixbuf_draw (win, gc, gui->image.b_light, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case MAGIC:
        pixbuf_draw (win, gc, gui->image.b_magic, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case EMPTY: break;
    default:
        perror ("Unknown color on ball_draw_from_pixbuf.");
        break;
    }
}

/* Display a resized version of the Pixbuf from a given ball.*/
void ball_draw_with_resize (GdkWindow * win, GdkGC * gc, Gui * gui, Ball ball, int resize_value)
{
    GdkPixbuf * falling_ball;
    switch (ball.color) 
    {
    case EARTH:
        falling_ball = gdk_pixbuf_scale_simple (gui->image.b_earth, resize_value, resize_value, GDK_INTERP_BILINEAR);
        pixbuf_draw (win, gc, falling_ball, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case FIRE:
        falling_ball = gdk_pixbuf_scale_simple (gui->image.b_fire, resize_value, resize_value, GDK_INTERP_BILINEAR);
        pixbuf_draw (win, gc, falling_ball, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case WIND:
        falling_ball = gdk_pixbuf_scale_simple (gui->image.b_wind, resize_value, resize_value, GDK_INTERP_BILINEAR);
        pixbuf_draw (win, gc, falling_ball, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case WATER:
        falling_ball = gdk_pixbuf_scale_simple (gui->image.b_water, resize_value, resize_value, GDK_INTERP_BILINEAR);
        pixbuf_draw (win, gc, falling_ball, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case LIFE:
        falling_ball = gdk_pixbuf_scale_simple (gui->image.b_life, resize_value, resize_value, GDK_INTERP_BILINEAR);
        pixbuf_draw (win, gc, falling_ball, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case DARKNESS:
        falling_ball = gdk_pixbuf_scale_simple (gui->image.b_darkness, resize_value, resize_value, GDK_INTERP_BILINEAR);
        pixbuf_draw (win, gc, falling_ball, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case LIGHT:
        falling_ball = gdk_pixbuf_scale_simple (gui->image.b_light, resize_value, resize_value, GDK_INTERP_BILINEAR);
        pixbuf_draw (win, gc, falling_ball, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case MAGIC:
        falling_ball = gdk_pixbuf_scale_simple (gui->image.b_magic, resize_value, resize_value, GDK_INTERP_BILINEAR);
        pixbuf_draw (win, gc, falling_ball, ball.x - BALL_SIZE / 2, ball.y - BALL_SIZE / 2);
        break;
    case EMPTY: break;
    default:
        perror ("Unknown color on ball_draw_from_pixbuf.");
        break;
    }
}

/* Draw the cannon with the given angle */
void cannon_draw (GdkWindow * win, Gui * gui)
{
    Image * image;
    Info * info;
    Cannon * cannon;
    double sx;
    double sy;
    cairo_matrix_t mat;  

    image = & gui->image;
    info = gui->info;
    cannon = & info->level.cannon;

    sx = - cairo_image_surface_get_width  (image->cannon) / 2.0;
    sy = - cairo_image_surface_get_height (image->cannon);

    info->cannon_cairo = gdk_cairo_create (win);
    cairo_save (info->cannon_cairo);

    cairo_matrix_init_identity (& mat);
    cairo_matrix_translate (& mat, cannon->x, cannon->y);
    cairo_matrix_rotate (& mat, cannon->angle);
    cairo_set_matrix (info->cannon_cairo, & mat);

    cairo_set_source_surface (info->cannon_cairo, image->cannon, sx, sy);
    cairo_paint (info->cannon_cairo);                   

    cairo_restore (info->cannon_cairo);
    cairo_destroy (info->cannon_cairo);
    info->cannon_cairo = NULL;
}

/* Draw of the base of the cannon over the cannon */
void cannon_base_draw (GdkWindow * win, GdkGC * gc, Gui * gui)
{
    Info * info;
    Image * image;
    int cannon_base_x, cannon_base_y;

    info = gui->info;
    image = & gui->image;

    cannon_base_x = (info->area_w - gdk_pixbuf_get_width (image->launch_panel)) / 2;
    cannon_base_y = info->area_h - gdk_pixbuf_get_height (image->launch_panel);

    pixbuf_draw (win, gc, gui->image.launch_panel, cannon_base_x, cannon_base_y);
}

/* Draw the next ball color behind the cover of the cannon */
void cannon_next_ball_draw (GdkWindow * win, GdkGC * gc, Info * info)
{
    Ball next_ball;

    next_ball = info->level.cannon.next_ball;

    switch (next_ball.color) 
    {
    case EARTH:
        change_color (gc, 0x6e4a44);
        gdk_draw_rectangle (win, gc, TRUE, (info->area_w - 12) / 2,
            info->area_h - 20, 12, 20);
        break;
    case FIRE:
        change_color (gc, 0x763d4b);
        gdk_draw_rectangle (win, gc, TRUE, (info->area_w - 12) / 2,
            info->area_h - 20, 12, 20);
        break;
    case WIND:
        change_color (gc, 0x8ca782);
        gdk_draw_rectangle (win, gc, TRUE, (info->area_w - 12) / 2,
            info->area_h - 20, 12, 20);
        break;
    case WATER:
        change_color (gc, 0x60908a);
        gdk_draw_rectangle (win, gc, TRUE, (info->area_w - 12) / 2,
            info->area_h - 20, 12, 20);
        break;
    case LIFE:
        change_color (gc, 0x6d897c);
        gdk_draw_rectangle (win, gc, TRUE, (info->area_w - 12) / 2,
            info->area_h - 20, 12, 20);
        break;
    case DARKNESS:
        change_color (gc, 0x38353b);
        gdk_draw_rectangle (win, gc, TRUE, (info->area_w - 12) / 2,
            info->area_h - 20, 12, 20);
        break;
    case LIGHT:
        change_color (gc, 0x988d78);
        gdk_draw_rectangle (win, gc, TRUE, (info->area_w - 12) / 2,
            info->area_h - 20, 12, 20);
        break;
    case MAGIC:
        change_color (gc, 0x3d4061);
        gdk_draw_rectangle (win, gc, TRUE, (info->area_w - 12) / 2,
            info->area_h - 20, 12, 20);
        break;
    case EMPTY: break;
    default:
        perror ("Unknown color on cannon_next_ball_draw.");
        break;
    }
}

/* Draw the flying elementary ball */
void cannon_ball_draw (GdkWindow * win, GdkGC * gc, Gui * gui)
{
    Cannon cannon;
    cannon = gui->info->level.cannon;

    ball_draw_from_pixbuf (win, gc, gui, cannon.ball);
}

/* Draw balls that are falling off the screen */
void falling_ball_draw (GdkWindow * win, GdkGC * gc, Gui * gui)
{
    Info * info;
    Ball ball;
    int cptr;

    info = gui->info;

    for (cptr = 0; cptr < info->nb_ball_falling - 1; cptr++)
    {
        ball = info->falling[cptr];
        ball_draw_with_resize (win, gc, gui, ball, BALL_SIZE - ball.y / 30);
    }
}

/* Draw a foreground with level and someinformations of the current game status */
void foreground_draw (GtkWidget * widget, GdkGC * gc, Gui * gui)
{
    Info * info;
    PangoLayout * playout;
    int width, height;

    info = gui->info;
    playout = gtk_widget_create_pango_layout (widget, NULL);
    font_set_name (playout, "Times, bold 14");
    width = (info->area_w - gdk_pixbuf_get_width (gui->image.foreground)) / 2;
    height = (info->area_h - gdk_pixbuf_get_height (gui->image.foreground)) / 2;

    change_color (gc, 0x3d2f37);

    switch (info->state)
    {
    case S_WIN :
        pixbuf_draw (widget->window, gc, gui->image.foreground, width, height);
        font_draw_text (widget, gc, playout, FONT_MC, info->area_w / 2, info->area_h / 2,
            "Congratulation !\nYou completed the %dth level.", gui->info->level_number + 1);
        break;
    case S_LOST :
        pixbuf_draw (widget->window, gc, gui->image.foreground, width, height);
        font_draw_text (widget, gc, playout, FONT_MC, info->area_w / 2, info->area_h / 2,
            "Oh no...\nYou failed at the %dth level.", gui->info->level_number + 1);
        break;
    case S_GAME_DONE :
        pixbuf_draw (widget->window, gc, gui->image.foreground, width, height);
        font_draw_text (widget, gc, playout, FONT_MC, info->area_w / 2, info->area_h / 2,
            "Wonderful !\nYou completed every single levels.");
        break;
    default : break;
    }
}

void elevator_draw (GdkWindow * win, GdkGC * gc, Gui * gui)
{
    Info * info;
    int line, column;
    info = gui->info;

    for (line = 0; line < info->level.elevator; line++)
    {
        for (column = 0; column <= NB_COLUMN; column++)
        {
            if (line == info->level.elevator - 1)
            {
                pixbuf_draw (win, gc, gui->image.elevator,
                gdk_pixbuf_get_width (gui->image.elevator) * column,
                line * (BALL_SIZE + BALL_GAP) * 0.8);
            }
            else
            {
                pixbuf_draw (win, gc, gui->image.elevator_top,
                gdk_pixbuf_get_width (gui->image.elevator_top) * column,
                line * (BALL_SIZE + BALL_GAP) * 0.8);
            }
        }
    }
}
