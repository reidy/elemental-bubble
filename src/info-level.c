/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#include "conf.h"

/* ============================= L E V E L   I N I T =============================== */

void first_level_init (Level * level)
{
    level->elevator = 1;
    level->same_ball_color = 0;
    level_init_from_file (level, 0);
}

void next_level_init (Info * info)
{
    Level * level;
    level = & info->level;

    level->elevator = 1;
    level->same_ball_color = 0;

    if (info->level_number < info->max_level_number)
    {
        level_init_from_file (level, info->level_number % info->max_level_number);
        info_reset (info);
    }
    else
    {
        info->state = S_GAME_DONE;
    }
}

void level_init_from_file (Level * level, int level_number)
{
    char file_line [100];
    int column, line, level_line;
    FILE * file;

    file = fopen (USER_FILE, "r");

    if (file == NULL)
    {
        perror ("Couldn't open the file on level_init_from_file");
        exit (0);
    }

    for (level_line = 0; level_line <= level_number; level_line++)
    {
        for (line = 0; line < NB_LINE; line++)
        {
            read_level_line (file, file_line);

            for (column = 0; column < NB_COLUMN; column++)
            {
                level->balls[column][line] = ball_init_from_file (level, file_line, column, line);
            }
        }
        /* Read blank line at end of the level */
        read_level_line (file, file_line);
    }
    fclose (file);
}

void read_level_line (FILE * file, char * file_line)
{
    if (fgets (file_line, 100, file) == NULL)
    {
        perror ("Level file corrupted, can't fully read the level on level_init_from_file.");
        exit (0);
    }
}


/* ============================= B A L L   I N I T =============================== */

Ball ball_init_from_file (Level * level, char * file_line, int column, int line)
{
    Ball ball;
    int place_in_line;
    int char_number;

    place_in_line = 2 * (line % 2) + column + column * 3;
    char_number = file_line [place_in_line] - '0';

    ball.x = ball_init_x (line, column);
    ball.y = ball_init_y (level, line);
    ball.color = ball_init_color (column, line, file_line [place_in_line], char_number);
    ball.state = ball_init_state (column, line, file_line [place_in_line]);

    return ball;
}

Ball_Color ball_init_color (int column, int line, char file_color, int char_number)
{
    return ((line % 2 && column == NB_COLUMN - 1) || (file_color < '0')) ? EMPTY : char_number;
}

Ball_State ball_init_state (int column, int line, char file_color)
{
    return ((line % 2 && column == NB_COLUMN - 1) || (file_color < '0')) ? REMOVED : TO_CHECK;
}

int ball_init_x (int line, int column)
{
    if (line % 2)
    {
        return BALL_GAP + BALL_SIZE / 2 + (BALL_SIZE + BALL_GAP) * column + (BALL_SIZE + BALL_GAP) / 2;
    }
    return BALL_GAP + BALL_SIZE / 2 + (BALL_SIZE + BALL_GAP) * column;
}

int ball_init_y (Level * level, int line)
{
    return BALL_GAP + BALL_SIZE / 2 + (BALL_SIZE + BALL_GAP) * 0.8 * line + level->elevator * (BALL_SIZE + BALL_GAP) * 0.8;
}

Ball ball_next_from_grid (Level * level)
{
    Ball ball;
    int column, line;
    int ball_count;
    int rand_ball;

    ball_count = level_ball_count (level);

    /* Take a random ball color */
    rand_ball = rand () % ball_count + 1;
    ball_count = 0;

    for (line = 0; line < NB_LINE; line++)
    {
        for (column = 0; column < NB_COLUMN; column++)
        {
            if (level->balls[column][line].color != EMPTY)
            {
                ball_count++;

                if (ball_count == rand_ball)
                {
                    ball.color = level->balls[column][line].color;
                }
            }
        }
    }

    ball.state = TO_CHECK;
    ball.x = level->cannon.x;
    ball.y = level->cannon.y;
    ball.dx = 0;
    ball.dy = 0;

    return ball;
}


/* ============================ L E V E L   D E B U G ============================== */

/* Count the number of balls on the grid */
int level_ball_count (Level * level)
{
    int ball_count;
    int column, line;

    ball_count = 0;

    for (line = 0; line < NB_LINE; line++)
    {
        for (column = 0; column < NB_COLUMN; column++)
        {
            if (level->balls[column][line].color != EMPTY)
            {
                ball_count++;
            }
        }
    }
    return ball_count;
}

int level_number_count (void)
{
    int line;
    int c;
    FILE * file;
    line = 0;

    file = fopen (USER_FILE, "r");

    if (file == NULL)
    {
        perror ("Couldn't open the file on level_number_count");
        exit (0);
    }

    while ((c = fgetc(file)) != EOF )
    {
        if (c == '\n')
            line++;
    }

    fclose (file);

    return line / (NB_LINE + 1);
}

void print_level_color (Level * level)
{
    int column, line;
    int ball_color;

    for (line = 0; line < NB_LINE; line++)
    {
        if (line % 2)
        {
            printf (" ");
        }
        for (column = 0; column < NB_COLUMN; column++)
        {
            ball_color = level->balls[column][line].color;
            (level->balls[column][line].color == 0) ? printf ("- ")
                                                     : printf ("%d ", ball_color);
        }
        printf ("\n");
    }
}

void print_level_state (Level * level)
{
    int column, line;

    for (line = 0; line < NB_LINE; line++)
    {
        if (line % 2)
        {
            printf (" ");
        }
        for (column = 0; column < NB_COLUMN; column++)
        {
            if (level->balls[column][line].state == REMOVED)
            {
                printf ("X ");
            }
            else if (level->balls[column][line].state == TO_CHECK)
            {
                printf ("C ");
            }
            else if (level->balls[column][line].state == CHECKED)
            {
                printf ("O ");
            }
            else if (level->balls[column][line].state == TO_REMOVE)
            {
                printf ("R ");
            }
        }
        printf ("\n");
    }
}

void print_ball (Level * level, int i, int j)
{
    Ball ball;
    ball.state = level->balls[i][j].state;
    ball.color = level->balls[i][j].color;
    printf ("ball at coordinates (%d,%d) has color (%s) and state(%s)\n", 
    i, j, get_ball_color (ball.color), get_ball_state (ball.state));
}

const char * get_ball_color (Ball_Color color) 
{
   switch (color) 
   {
      case EARTH: return "EARTH";
      case FIRE: return "FIRE";
      case WIND: return "WIND";
      case WATER: return "WATER";
      case LIFE: return "LIFE";
      case DARKNESS: return "DARKNESS";
      case LIGHT: return "LIGHT";
      case MAGIC: return "MAGIC";
      case EMPTY: return "EMPTY";
      default: return "ERROR_COLOR";
   }
}

const char * get_ball_state (Ball_Color state) 
{
   switch (state) 
   {
      case TO_CHECK: return "TO_CHECK";
      case CHECKED: return "CHECKED";
      case TO_REMOVE: return "TO_REMOVE";
      case REMOVED: return "REMOVED";
      default: return "ERROR_STATE";
  }
}    
