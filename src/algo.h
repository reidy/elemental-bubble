/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#ifndef __ALGO_H__
#define __ALGO_H__

#include "conf.h"

#define NB_ADJACENT_BALL 6

/* Removability check */

void algo_remove_balls (Info *info, int ball_column, int ball_line,
                        Ball_Color color_new_ball);
void find_removable (Info * info, int ball_column, int ball_line,
                     Ball_Color color_new_ball);
void get_staying (Info * info, int ball_column, int ball_line);
void ball_remove_checker (Info * info);
void clean_non_attached (Info * info);

/* Collision */
int collision_test (Ball ball1, Ball ball2);
Point detect_collision (Info * info, int ball_column, int ball_line);

/* Common tools */
void fill_d_line (int * d_line);
void fill_d_column (int * d_column, int ball_line);
void clean_state_grid (Info * info);
void remove_ball (Info * info, Ball * ball);
gboolean loss_test (Info * info);

#endif
