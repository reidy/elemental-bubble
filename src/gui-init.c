/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#include "conf.h"


/* ============================== G U I   I N I T ================================= */

void gui_init (Gui * g, Info * info)
{
    g->magic = GUI_MAGIC;
    g->info = info;

    gui_init_window (g);
    gui_init_layout (g);
    gui_init_top (g);
    gui_init_images (g);
    gui_init_area   (g);
    gui_init_statusbar (g);
    printf ("Gui init... [OK].\n");
}


/* =============================== W I N D O W S ================================== */

void gui_init_window (Gui * g)
{
    g->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (g->window), "Elemental Bubbles");
    gtk_window_set_default_size (GTK_WINDOW (g->window), g->info->area_w, 454);
    gtk_widget_set_size_request (g->window, g->info->area_w, 454);
    gtk_window_set_resizable (GTK_WINDOW (g->window), FALSE);

    ON_SIGNAL (g->window, "delete-event", on_window_delete_event, g);
    ON_SIGNAL (g->window, "destroy",      on_window_destroy,      g);
}

void on_window_destroy (GtkWidget U * widget, gpointer U data)
{
    printf ("Gtk destroying...");
    gtk_main_quit ();
    printf ("[OK]\n");
}

gboolean on_window_delete_event (GtkWidget U * widget, GdkEvent U *event, 
    gpointer U data)
{
    printf ("Destroy the window.\n");
    return FALSE;
}


/* ================================ L A Y O U T =================================== */

void gui_init_layout (Gui * g)
{
    Layout * l;
    l = & g->layout;

    l->main_box = gtk_vbox_new (!HOMOGENEOUS, 0);
    l->top_box  = gtk_hbox_new (!HOMOGENEOUS, 0);

    gtk_container_add (GTK_CONTAINER (g->window), l->main_box);
    gtk_box_pack_start (GTK_BOX (l->main_box), l->top_box, !EXPAND, !FILL, 0);
}

void gui_init_top (Gui * g)
{
    Layout * l;
    l = & g->layout;

    l->startbutton = gtk_button_new_with_label ("Start");
    gtk_box_pack_start (GTK_BOX (l->top_box), l->startbutton, !EXPAND, !FILL, 0);
    l->resetbutton = gtk_button_new_with_label ("Reset");
    gtk_box_pack_start (GTK_BOX (l->top_box), l->resetbutton, !EXPAND, !FILL, 0);
    l->quitbutton = gtk_button_new_with_label ("Quit");
    gtk_box_pack_end (GTK_BOX (l->top_box), l->quitbutton, !EXPAND, !FILL, 0);

    ON_SIGNAL (l->startbutton, "clicked", on_start_clicked, g);
    ON_SIGNAL (l->resetbutton, "clicked", on_reset_clicked, g);
    ON_SIGNAL (l->quitbutton, "clicked", on_window_destroy, g);
}


/* ================================ I M A G E S =================================== */

void gui_init_images (Gui * g)
{
    Image * image;
    image = & g->image;

    image->b_fire = pixbuf_load ("./Data/Ball_Fire.png");
    image->b_earth = pixbuf_load ("./Data/Ball_Earth.png");
    image->b_magic = pixbuf_load ("./Data/Ball_Magic.png");
    image->b_darkness = pixbuf_load ("./Data/Ball_Dark.png");
    image->b_life = pixbuf_load ("./Data/Ball_Life.png");
    image->b_water = pixbuf_load ("./Data/Ball_Water.png");
    image->b_light = pixbuf_load ("./Data/Ball_Light.png");
    image->b_wind = pixbuf_load ("./Data/Ball_Wind.png");

    image->background = pixbuf_load ("./Data/Background.png");
    image->foreground = pixbuf_load ("./Data/Foreground.png");
    image->cannon = cairo_image_surface_create_from_png ("Data/Cannon.png");
    image->launch_panel = pixbuf_load ("./Data/Launch_Panel.png");
    image->elevator = pixbuf_load ("./Data/Elevator.png");
    image->elevator_top = pixbuf_load ("./Data/Elevator_Top.png");
}


/* ======================== O N   A R E A   E V E N T S =========================== */

void gui_init_area (Gui * g)
{
    Layout * l = & g->layout;

    l->area  = gtk_drawing_area_new ();
    gtk_box_pack_start (GTK_BOX (l->main_box), l->area, EXPAND, FILL, 3);

    GTK_WIDGET_SET_FLAGS  (l->area, GTK_CAN_FOCUS);
    gtk_widget_add_events (l->area, GDK_BUTTON_MOTION_MASK    |
                                    GDK_BUTTON_PRESS_MASK    |
                                    GDK_BUTTON_RELEASE_MASK    |
                                    GDK_KEY_PRESS_MASK        |
                                    GDK_KEY_RELEASE_MASK    |
                                    GDK_EXPOSURE_MASK        );

    ON_SIGNAL (l->area, "expose-event",            on_area_expose,            g);
    ON_SIGNAL (l->area, "key-press-event",        on_area_key_press,        g);
    ON_SIGNAL (l->area, "key-release-event",    on_area_key_release,    g);
}

/* ============================ S T A T U S   B A R =============================== */

void gui_init_statusbar (Gui * g)
{
    Layout * l = & g->layout;
 
    g->statusbar = gtk_statusbar_new ();
    statusbar_put (GTK_STATUSBAR (g->statusbar), "Welcome !");
    gtk_box_pack_start (GTK_BOX (l->main_box), g->statusbar, !EXPAND, !FILL, 0);
}


/* ============================= G U I   D E B U G ================================ */

/* This function test if the data is a Gui, else it means that:
 *  - or we forgot to add gui on a g_signal_connect,
 *  - or we didn't add a good signal handler.
*/

Gui * gui_check (gpointer data)
{
    if (data == NULL)
    { 
        perror ("Data is NULL on gui_check.\n");
        return NULL; 
    }
    if (((Gui *)data)->magic != GUI_MAGIC)
    { 
        perror ("Wrong magic number on gui_check.\n");
        return NULL;
    }

    return data;
}
