/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#include "conf.h"

/* Gui init */
void gui_init (Gui * g, Info * info);
void gui_init_window (Gui * g);
void on_window_destroy (GtkWidget U * widget, gpointer U data);
gboolean on_window_delete_event (GtkWidget U * widget, GdkEvent U *event, gpointer U data);
void gui_init_layout (Gui * g);
void gui_init_top (Gui * g);
void gui_init_images (Gui * g);
void gui_init_area (Gui * g);
void gui_init_statusbar (Gui * g);
Gui * gui_check (gpointer data);
