/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#ifndef __INFO_LEVEL_H__
#define __INFO_LEVEL_H__

#include "conf.h"

/* Level init */
void first_level_init (Level * level);
void next_level_init (Info * info);
void level_init_from_file (Level * level, int level_number);
void read_level_line (FILE * file, char * file_line);

/* Ball init */
Ball ball_init_from_file (Level * level, char * file_line, int column, int line);
Ball_Color ball_init_color (int column, int line, char file_color, int char_number);
Ball_State ball_init_state (int column, int line, char file_color);
int ball_init_x (int line, int column);
int ball_init_y (Level * level, int line);
Ball ball_next_from_grid (Level * level);

/* Level debug */
int level_ball_count (Level * level);
int level_number_count (void);
void print_level_color (Level * level);
void print_level_state (Level * level);
void print_ball (Level * level, int i, int j);
const char * get_ball_color (Ball_Color color);
const char * get_ball_state (Ball_Color state);

#endif
