/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#include "conf.h"

/* ============== R E M O V A B I L I T Y   C H E C K =============== */

void algo_remove_balls (Info *info, int ball_column, int ball_line,
                        Ball_Color color_new_ball)
{
    int column;

    find_removable (info, ball_column, ball_line, color_new_ball);

    printf ("Before\n");
    print_level_state (&info->level);

    for (column = 0; column < NB_COLUMN; column++)
    {
        if (info->level.balls[column][0].state != TO_REMOVE
         && info->level.balls[column][0].color != EMPTY)
        {
            info->level.balls[column][0].state = CHECKED;
            get_staying (info, column, 0);
        }
    }
    printf ("After\n");
    print_level_state (&info->level);

    ball_remove_checker (info);

    info->level.same_ball_color = 0;
}

/* Checks how many balls of the same color there are near the landing
 * ball */
void find_removable (Info *info, int ball_column, int ball_line,
                     Ball_Color color_new_ball)
{
    int d_line[NB_ADJACENT_BALL], d_column[NB_ADJACENT_BALL];
    int i, near_column, near_line;

    fill_d_line (d_line);
    fill_d_column (d_column, ball_line);

    for (i = 0; i < NB_ADJACENT_BALL; i++)
    {
        near_column = ball_column + d_column[i];
        near_line = ball_line + d_line[i];

        if ((info->level.balls[near_column][near_line].color
                == color_new_ball)
            && (info->level.balls[near_column][near_line].state
                != TO_REMOVE))
        {
            info->level.same_ball_color++;
            info->level.balls[near_column][near_line].state = TO_REMOVE;
            find_removable (info, near_column, near_line,
                       info->level.balls[near_column][near_line].color);
        }
    }
}

/* Changes the state of the balls that will not fall after the chain of
 * same color ball is removed */
void get_staying (Info *info, int ball_column, int ball_line)
{
    int d_line[NB_ADJACENT_BALL], d_column[NB_ADJACENT_BALL];
    int i, near_column, near_line;

    fill_d_line (d_line);
    fill_d_column (d_column, ball_line);

    for (i = 0; i < NB_ADJACENT_BALL; i++)
    {
        near_column = ball_column + d_column[i];
        near_line = ball_line + d_line[i];

        if (info->level.balls[near_column][near_line].color != EMPTY &&
            info->level.balls[near_column][near_line].state == TO_CHECK)
        {
            info->level.balls[near_column][near_line].state = CHECKED;
            get_staying (info, near_column, near_line);
        }
    }
}

/* Check which ball will be removed and call the required functions */
void ball_remove_checker (Info * info)
{
    Ball *ball;
    int ball_column, ball_line;

    if (info->level.same_ball_color < 3)
    {
        return;
    }

    for (ball_line = 0; ball_line < NB_LINE; ball_line++)
    {
        for (ball_column = 0; ball_column < NB_COLUMN; ball_column++)
        {
            ball = & info->level.balls[ball_column][ball_line];

            if (ball->state == TO_REMOVE || ball->state == TO_CHECK)
            {
                remove_ball (info, ball);
            }
        }
    }
    clean_non_attached (info);
}

void clean_non_attached (Info * info)
{
    Ball * ball;
    Ball neighbor;
    int column, line, cptr;
    int d_column[6], d_line[6];
    int free_cell;

    for (line = 0; line < NB_LINE; line++)
    {
        for (column = 0; column < NB_COLUMN; column++)
        {
            free_cell = 0;

            fill_d_column (d_column, line);
            fill_d_line (d_line);
            ball = & info->level.balls[column][line];

            if (ball->color != EMPTY && line > info->level.elevator)
            { 
                for (cptr = 0; cptr < 6; cptr++)
                {
                    neighbor = info->level.balls[column + d_column[cptr]][line + d_line[cptr]];
                    if (neighbor.color == EMPTY)
                    {
                        free_cell++;
                    }
                }

                if (free_cell == 6)
                {
                    remove_ball (info, ball);
                }
            }
        }
    }
}


/* ============================== C O L L I S I O N =============================== */

/* Dist function to compare the dist of two balls from their center */
int collision_test (Ball b1, Ball b2)
{
    float calc;
    calc = sqrt ((b1.x - b2.x) * (b1.x - b2.x) + (b1.y - b2.y) * (b1.y - b2.y));

    return (calc < ((BALL_GAP + BALL_SIZE) * 0.8)) ? TRUE : FALSE;
}

/* Return both coordinate from the point with -1 if no collision were found,
 * else it returns the coordinate from the point where the ball need to go */
Point detect_collision (Info * info, int ball_column, int ball_line)
{
    Point point;
    Level * level;
    Ball ball;
    int column, line;

    level = & info->level;
    for (line = 0; line < NB_LINE; line++)
    {
        for (column = 0; column < NB_COLUMN; column++)
        {
            ball = level->balls[ball_column][ball_line];
            if (ball_line <= 0 && ball.color == EMPTY)
            {
                point.column = ball_column;
                point.line = ball_line; 
                return point;
            }
            if (collision_test (level->cannon.ball, level->balls[column][line])
                && level->balls[column][line].color != EMPTY
                && ball.color == EMPTY)
            {
                point.column = ball_column;
                point.line = ball_line;
                return point;
            }
        }
    }
    point.column = -1;
    point.line = -1;
    return point;
}


/* =========================== C O M M O N   T O O L S ============================ */

/* Fill the neighbor table with adjacent lines */
void fill_d_line (int * d_line)
{
    d_line[0] = 0;
    d_line[1] = 0;
    d_line[2] = -1;
    d_line[3] = 1;
    d_line[4] = -1;
    d_line[5] = 1;
}

/* Fill the neighbor table with adjacent columns */
void fill_d_column (int * d_column, int ball_line)
{
    int s;
    s = (ball_line % 2) ? 1 : -1;

    d_column[0] = -1;
    d_column[1] = 1;
    d_column[2] = 0;
    d_column[3] = 0;
    d_column[4] = s;
    d_column[5] = s;
}

/* Clean the state of every balls on the level */
void clean_state_grid (Info * info)
{
    int column, line;
    Level * level;

    level = & info->level;

    for (line = 0; line < NB_LINE; line++)
    {
        for (column = 0; column < NB_COLUMN; column++)
        {
            level->balls[column][line].state = TO_CHECK;
        }
    }
}

/* Clean the state and color from a ball and update its speed vector to fall down */
void remove_ball (Info * info, Ball * ball)
{
    ball->dx = rand() % 6 - 3;
    ball->dy = rand() % 4 + 4;

    info->falling[info->nb_ball_falling] = * ball;
    info->nb_ball_falling++;

    ball->state = REMOVED;
    ball->color = EMPTY;
}

/* Remove all of the balls from the game */
void remove_all_ball (Info * info)
{
    int column, line;

    for (line = 0; line < NB_LINE; line++)
    {
        for (column = 0; column < NB_COLUMN; column++)
        {
            remove_ball (info, & info->level.balls[column][line]);
        }
    }
}

/* Test if the player loosed the game */
gboolean loss_test (Info * info)
{
    Level * level;
    int column;

    level = & info->level;
    
    for (column = 0; column < NB_COLUMN; column++)
    {
        if (level->balls[column][NB_LINE - level->elevator].color != EMPTY)
        {
            remove_all_ball (info);
            return TRUE;
        }
    }
    return FALSE;
}
