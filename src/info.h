/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#include "conf.h"

/* Info init */
void info_init (Info * info);
void area_size_init (Info * info);
void cannon_init (Info * info);
void cannon_balls_init (Level * level);
void cairo_init (Info * info);
void info_reset (Info * info);

/* State */
void state_show (Info * info, GtkWidget * statusbar);
void state_set (Info * info, Game_State state, GtkWidget * statusbar);
