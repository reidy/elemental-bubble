/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#ifndef __CONF_H__
#define __CONF_H__

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include <cairo.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <gdk-pixbuf/gdk-pixbuf.h>


#define USER_FILE "Data/level"
#define GUI_MAGIC 0x46EA7E05

#define NB_COLUMN 8
#define NB_LINE 11

#define BALL_SIZE 32
#define BALL_GAP 4
#define BALL_SPEED 4.5
#define MAX_CANNON_VALUE 150

/* Area redraw timeout */
#define TIMEOUT1 30
/* Level events timeout */
#define TIMEOUT2 10

#define HOMOGENEOUS    TRUE
#define EXPAND        TRUE
#define FILL        TRUE

#define U __attribute__((unused))
#define ON_SIGNAL(w,n,c,d) \
    (g_signal_connect(G_OBJECT(w), (n), G_CALLBACK(c), (d) ))


/* A point, two coordinates */
typedef struct
{
    int column;
    int line;
} Point;

/* Different states that can have the ball from the cannon */
typedef enum {
    S_BALL_IN_CANNON,
    S_BALL_LAUNCHED,
    S_LOST,
    S_WIN,
    S_GAME_DONE
} Game_State;

/* Possible color value that a ball can use */
typedef enum
{
    EMPTY, EARTH, FIRE, WIND,
    WATER, LIFE, DARKNESS,
    LIGHT, MAGIC
} Ball_Color;

/* Different state of the ball used on the algorithm */
typedef enum
{
    REMOVED,                    /* Has been removed */
    TO_CHECK,                    /* Need to be checked */
    CHECKED,                    /* Has been checked */
    TO_REMOVE                    /* Need to be removed */
} Ball_State;

/* Ball parameters */
typedef struct
{
    Ball_State state;            /* Store the state of the ball */
    Ball_Color color;            /* Store the color of the ball */
    float x, y;
    float dx, dy;
} Ball;

/* Cannon parameters */
typedef struct
{
    Ball ball;                    /* Ball to launch */
    Ball next_ball;                /* Next ball to be launch, placed on the bottom */
    float angle;                /* Cannon's angle in radian */
    int turn_value;                /* Angle's turn value */
    int x, y;                    /* x, y coordinate of the cannon's base */
    int dx, dy;
} Cannon;

/* Level parameters */
typedef struct
{
    Ball balls [NB_COLUMN][NB_LINE];    /* Balls are managed on a 2D table of NB_COLUMN * NB_LINE */
    int same_ball_color;        /* Number of balls of the same color in a row */
    int elevator;                /* How far down the ceiling went */
    Cannon cannon;
} Level;

/* Game informations */
typedef struct
{
    Level level;                /* To load a level can be loaded */
    int area_h, area_w;            /* Height and width of the area */
    cairo_t * cannon_cairo;        /* Cairo context for the cannon */
    gint timeout1, timeout2;    /* Timeouts */
    gboolean anim;                /* Ball animation boolean */
    gboolean cannon_turn_left;    /* Know if left key is pressed */
    gboolean cannon_turn_right;    /* Know if right key is pressed */
    Game_State state;            /* Game state */
    int level_number;            /* Actual level number */
    int max_level_number;        /* Max level that can be loaded */
    int nb_ball_falling;        /* Actual number of ball on the falling table */
    Ball falling[NB_COLUMN * NB_LINE];    /* Table for drawing falling balls */
    int nb_ball_launch;            /* Total nb of balls launched during the level */
} Info;

/* Layout */
typedef struct
{
    GtkWidget * main_box, * top_box;
    GtkWidget * startbutton;
    GtkWidget * resetbutton;
    GtkWidget * quitbutton;
    GtkWidget * area;
} Layout;

/* Images loaded */
typedef struct
{
    GdkPixbuf * b_earth, * b_fire, * b_magic;
    GdkPixbuf * b_darkness, * b_life, * b_water;
    GdkPixbuf * b_light, * b_wind;
    GdkPixbuf * background;
    GdkPixbuf * foreground;
    GdkPixbuf * launch_panel;
    GdkPixbuf * elevator;
    GdkPixbuf * elevator_top;
    cairo_surface_t * cannon;
} Image;

/* Game graphical user interface */
typedef struct
{
    guint magic;                /* Magic number to know if the struct is a GUI */
    Info * info;                /* Game informations */
    Layout layout;                /* Gui layout */
    Image image;                /* Image loaded through the displa */
    GtkWidget * window;            /* Main window */
    GtkWidget * statusbar;        /* Status bar which display the game state */
} Gui;


#include "algo.h"
#include "font.h"
#include "gui-draw.h"
#include "gui-event.h"
#include "gui-init.h"
#include "info.h"
#include "info-anim.h"
#include "info-level.h"
#include "util.h"

#endif
