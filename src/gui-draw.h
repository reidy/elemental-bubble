/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#include "conf.h"

/* Drawing area */
gboolean on_area_expose (GtkWidget * widget, GdkEventExpose U * e, gpointer U data);
void level_draw (GdkWindow * win, GdkGC * gc, Gui * gui);
void cannon_draw (GdkWindow * win, Gui * gui);
void ball_draw_from_pixbuf (GdkWindow * win, GdkGC * gc, Gui * gui, Ball ball);
void ball_draw_with_resize (GdkWindow * win, GdkGC * gc, Gui * gui, Ball ball, int resize_value);
void cannon_base_draw (GdkWindow * win, GdkGC * gc, Gui * gui);
void cannon_next_ball_draw (GdkWindow * win, GdkGC * gc, Info * info);
void cannon_ball_draw (GdkWindow * win, GdkGC * gc, Gui * gui);
void falling_ball_draw (GdkWindow * win, GdkGC * gc, Gui * gui);
void foreground_draw (GtkWidget * widget, GdkGC * gc, Gui * gui);
void elevator_draw (GdkWindow * win, GdkGC * gc, Gui * gui);
