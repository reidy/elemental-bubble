/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#include "conf.h"


/* ================================ T I M E O U T ================================= */

/* Timeout used to update the drawing area */
gboolean on_timeout1 (gpointer data)
{
    Gui * gui;
    gui = gui_check (data);

    area_redraw (gui->layout.area);
    return TRUE;
}

/* Timeout used to modify any level elements */
gboolean on_timeout2 (gpointer data)
{
    Gui * gui;
    Info * info;
    gui = gui_check (data);
    info = gui->info;

    switch (info->state)
    {
    case S_BALL_IN_CANNON :
        cannon_next_step (info);
        cannon_update (info);
        cannon_ball_update_pos (& info->level);
        break;
    case S_BALL_LAUNCHED :
        cannon_next_step (info);
        cannon_update (info);
        on_ball_launch (info);
        break;
    default :
        break;
    }
    level_timer_next_step (gui, info);

    return TRUE;
}


/* ============================== A N I M A T I O N =============================== */

/* Start button click event */
void info_anim_start (Info * info, gpointer data)
{
    Gui * gui;
    gui = gui_check (data);

    if (info->timeout1 == 0)
    {
        info->timeout1 = g_timeout_add (TIMEOUT1, on_timeout1, data);
    }

    if (info->timeout2 == 0)
    {
        info->timeout2 = g_timeout_add (TIMEOUT2, on_timeout2, data);
    }

    gtk_widget_grab_focus (gui->layout.area);
}

/* Stop button click event */
void info_anim_stop (Info * info)
{
    g_source_remove (info->timeout1);

    if (info->timeout1 != 0)
    {
        g_source_remove (info->timeout1);
        info->timeout1 = 0;
    }

    if (info->timeout2 != 0)
    {
        g_source_remove (info->timeout2);
        info->timeout2 = 0;
    }
}

/* Update the dynamic array used to store every falling balls */
void falling_balls (Info * info)
{
    int cptr;

    for (cptr = 0; cptr < info->nb_ball_falling; cptr++)
    {
        if (falling_ball_animation (info, & info->falling[cptr]) == 0)
        {
            info->falling[cptr] = info->falling[--info->nb_ball_falling];
        }
    }
}

/* Condition and incrementation of the ball animation */
gboolean falling_ball_animation (Info * info, Ball * ball)
{
    if ((ball->x + BALL_SIZE / 2 > 0)
        && (ball->x - BALL_SIZE / 2 < info->area_w)
        && (ball->y - BALL_SIZE / 2 < info->area_h))
    {
        ball->x -= ball->dx;
        ball->y += ball->dy;
        return TRUE;
    }
    return FALSE;
}


/* ============================= C H E C K   I N F O ============================== */

/* Ball launch event, check if the ball can be spinned to the grid */
void on_ball_launch (Info * info)
{
    Level * level;
    Ball ball;
    int ball_column, ball_line;

    level = & info->level;
    ball = level->cannon.ball;

    ball_line = ball_pos_line_from_y (level, ball);
    ball_column = ball_pos_column_from_x (level, ball);

    clean_state_grid (info);
    ball_next_step (info);

    if (add_grid_ball (info, ball_column, ball_line))
    {
        level_update (info, ball_column, ball_line);
    }
}

/* Add a new ball on the grid if the return value from detect_collision
 * is not -1,-1 */
gboolean add_grid_ball (Info * info, int ball_column, int ball_line)
{
    Point p;
    Level * level;
    Cannon * cannon;

    level = & info->level;
    cannon = & level->cannon;

    p = detect_collision (info, ball_column, ball_line);

    if (p.column != -1 && p.line != -1)
    {
        level->balls[p.column][p.line] = cannon->ball;
        level->balls[ball_column][ball_line].state = TO_CHECK;
        level->balls[p.column][p.line].x = ball_init_x (p.line, p.column);
        level->balls[p.column][p.line].y = ball_init_y (level, p.line);

        return 1;
    }

    return 0;
}


/* ============================= N E X T   S T E P S ============================== */

/* Change the rotation angle from the cannon according to which key is pressed */
void cannon_next_step (Info * info)
{
    Cannon * cannon;
    cannon = & info->level.cannon;

    if (info->cannon_turn_left == info->cannon_turn_right)
    {
        return;
    }
    if (info->cannon_turn_left == 1)
    {
        cannon->turn_value = (--cannon->turn_value < 0) ? 0 : cannon->turn_value;
    }
    if (info->cannon_turn_right == 1)
    {
        cannon->turn_value = (++cannon->turn_value > MAX_CANNON_VALUE)
                                ? MAX_CANNON_VALUE : cannon->turn_value;
    }
}

/* Update the position of the ball at the end of the cannon */
void cannon_ball_update_pos (Level * level)
{
    Cannon * cannon;
    Ball * ball;
    cannon = & level->cannon;
    ball = & cannon->ball;

    ball->x = BALL_SIZE / 2 + cannon->dx - BALL_SIZE / 2;
    ball->y = BALL_SIZE / 2 + cannon->dy - BALL_SIZE / 2;
    ball->dx = sin (cannon->angle) * BALL_SPEED;
    ball->dy = - cos (cannon->angle) * BALL_SPEED;
}

/* Angle from 30° to 160°, the static 55 value is the cannon height in pixels */
void cannon_update (Info * info)
{
    Cannon * cannon;
    cannon = & info->level.cannon;

    cannon->angle = cannon->turn_value / 150.0 * 2 * G_PI / 3 - G_PI / 3;
    cannon->dx = cannon->x + sin (cannon->angle) * 55;
    cannon->dy = cannon->y - cos (cannon->angle) * 55;
}

/* Reverse the speed vector if the ball hit a vertical side */
void ball_next_step (Info * info)
{
    Ball * ball;
    ball = & info->level.cannon.ball;

    ball->x += ball->dx;
    ball->y += ball->dy;

    if (ball->x - BALL_SIZE / 2 < 0)
    {
        ball->x = BALL_SIZE / 2;
        ball->dx = - ball->dx;
    }
    if (ball->x + BALL_SIZE / 2 > info->area_w)
    {
        ball->x = info->area_w - BALL_SIZE / 2;
        ball->dx = - ball->dx;
    }
}

/*Update the position from every balls on the grid after an elevator change */
void ball_grid_update_pos (Level * level)
{
    int column, line;

    for (line = 0; line < NB_LINE; line++)
    {
        for (column = 0; column < NB_COLUMN; column++)
        {
            level->balls[column][line].x = ball_init_x (line, column);
            level->balls[column][line].y = ball_init_y (level, line);
        }
    }
}

/* Run level-dedicated tasks for the timer */
void level_timer_next_step (Gui * gui, Info * info)
{
    Level * level;
    level = & info->level;

    if (info->nb_ball_launch == 15)
    {
        level->elevator++;
        ball_grid_update_pos (level);
        loss_test (info);
        info->nb_ball_launch = 0;
    }

    falling_balls (info);
    state_show (info, gui->statusbar);
    area_redraw (gui->layout.area);
}

/* Launch algorithm and update/remove status from the removed balls */
void level_update (Info * info, int ball_column, int ball_line)
{
    Level * level;
    Ball ball;

    level = &info->level;
    ball = level->balls[ball_column][ball_line];

    algo_remove_balls (info, ball_column, ball_line, ball.color);

    if (loss_test (info))
    {
        info->state = S_LOST;
        level->cannon.ball.color = EMPTY;
    }
    else if (level_ball_count (& info->level) == 0)
    {
        info->state = S_WIN;
        level->cannon.ball.color = EMPTY;
    }
    else
    {
        level->cannon.ball = level->cannon.next_ball;
        level->cannon.next_ball = ball_next_from_grid (level);
        cannon_ball_update_pos (level);
        info->nb_ball_launch++;

        info->state = S_BALL_IN_CANNON;
    }
}
