/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#include "conf.h"

int main (int argc, char * argv [])
{
    Gui gui;
    Info info;

    gtk_init (& argc, & argv);

    srand (time (NULL));
    info_init (& info);

    gui_init (& gui, &info);
    gtk_widget_show_all (gui.window);
    
    gtk_main ();

    return EXIT_SUCCESS;
}
