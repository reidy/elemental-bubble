/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#include "conf.h"

/* ============================== I N F O   I N I T =============================== */

void info_init (Info * info)
{
    info->level_number = 0;
    info->max_level_number = level_number_count ();

    first_level_init (& info->level);
    area_size_init (info);
    cannon_init (info);
    cairo_init (info);

    info->anim = 0;
    info->timeout1 = 0;
    info->timeout2 = 0;
    info->cannon_turn_left = 0;
    info->cannon_turn_right = 0;
    info->state = S_BALL_IN_CANNON;
    info->nb_ball_falling = 0;
    info->nb_ball_launch = 0;
}

void area_size_init (Info * info)
{
    info->area_w = 292;
    info->area_h = 400;
}

void cannon_init (Info * info)
{
    Cannon * cannon;
    cannon = & info->level.cannon;

    cannon->angle = 2 * G_PI;
    cannon->turn_value = MAX_CANNON_VALUE / 2;
    cannon->x = info->area_w / 2;
    cannon->y = info->area_h;
    cannon->dx = cannon->x + sin (cannon->angle) * 55;
    cannon->dy = cannon->y - cos (cannon->angle) * 55;
    cannon_balls_init (& info->level);
}

void cannon_balls_init (Level * level)
{
    level->cannon.ball = ball_next_from_grid (level);
    cannon_ball_update_pos (level);
    level->cannon.next_ball = ball_next_from_grid (level);
}

void cairo_init (Info * info)
{
    info->cannon_cairo = NULL;
}

void info_reset (Info * info)
{
    Level * level;
    Cannon * cannon;
    level = & info->level;
    cannon = & level->cannon;

    cannon->angle = 2 * G_PI;
    cannon->turn_value = MAX_CANNON_VALUE / 2;
    cannon->dx = cannon->x + sin (cannon->angle) * 55;
    cannon->dy = cannon->y - cos (cannon->angle) * 55;
    cannon_ball_update_pos (level);

    level->cannon.ball = ball_next_from_grid (level);
    level->cannon.next_ball = ball_next_from_grid (level);
    level->elevator = 1;

    info->nb_ball_launch = 0;
    info->cannon_turn_left = 0;
    info->cannon_turn_right = 0;
    info->nb_ball_falling = 0;
    info->state = S_BALL_IN_CANNON;
}


/* ================================== S T A T E =================================== */

void state_show (Info * info, GtkWidget * statusbar)
{
    if (info->anim == 1)
    {
        switch (info->state)
        {
        case S_BALL_IN_CANNON :
            statusbar_put (GTK_STATUSBAR (statusbar), "Move the cannon with your arrows.");
            break;
        case S_BALL_LAUNCHED :
            statusbar_put (GTK_STATUSBAR (statusbar), "Ball launched...");
            break;
        case S_LOST :
            statusbar_put (GTK_STATUSBAR (statusbar), "Press espace to continue.");
            break;
        case S_WIN :
            statusbar_put (GTK_STATUSBAR (statusbar), "Press espace to continue.");
            break;
        case S_GAME_DONE :
            statusbar_put (GTK_STATUSBAR (statusbar), "You completed every level, congrats!");
            break;
        default : break;
        }
    }
    else
    {
        statusbar_put (GTK_STATUSBAR (statusbar), "Press Start.");
    }
}

void state_set (Info * info, Game_State state, GtkWidget * statusbar)
{
    info->state = state;
    state_show (info, statusbar);
}
