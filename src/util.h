/*
 * Copyright © 2014 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#ifndef __UTIL_H__
#define __UTIL_H__

#include "conf.h"

/* Color */
void set_color (GdkGC *gc, int r, int g, int b);
void change_color (GdkGC * gc, int hexcode);

/* Frames */
GtkWidget * frame_new (gchar * label, gboolean shadowed);

/* Status bar */
void statusbar_put (GtkStatusbar * bar, char * text);

/* Area redraw */
void area_redraw (GtkWidget * area);

/* Image draw */
void pixbuf_get_size (GdkPixbuf * pix, int * w, int * h);
void pixbuf_draw (GdkWindow * win, GdkGC * gc, GdkPixbuf * pix, int x, int y);
GdkPixbuf * pixbuf_load (const char * filename);

/* Ball positions */
int ball_pos_column_from_x (Level * level, Ball ball);
int ball_pos_line_from_y (Level * level, Ball ball);

#endif
