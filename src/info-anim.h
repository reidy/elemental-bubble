/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#include "conf.h"

/* Timeouts */
gboolean on_timeout1 (gpointer data);
gboolean on_timeout2 (gpointer data);
gboolean on_timeout3 (gpointer data);

/* Animations */
void info_anim_start (Info * info, gpointer data);
void info_anim_stop (Info * info);
void falling_balls (Info * info);
gboolean falling_ball_animation (Info * info, Ball * ball);

/* Check info */
void on_ball_launch (Info * info);
gboolean add_grid_ball (Info * info, int ball_column, int ball_line);

/* Next steps */
void cannon_next_step (Info * info);
void cannon_ball_update_pos (Level * level);
void cannon_update (Info * info);
void ball_next_step (Info * info);
void ball_grid_update_pos (Level * level);
void level_timer_next_step (Gui * gui, Info * info);
void level_update (Info * info, int ball_column, int ball_line);
