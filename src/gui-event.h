/*
 * Copyright © 2014-2015 Reid & Sykefu
 * Reid [at] tuxfamily.org
 *
 * This program is free software under the terms of the
 * GNU Lesser General Public License (LGPL) version 2.1.
*/

#include "conf.h"

/* Click event */
void on_start_clicked (GtkButton U * widget, gpointer U data);
void on_reset_clicked (GtkButton U * widget, gpointer U data);

/* Key event */
gboolean on_area_key_espace_press (GtkWidget U * w, gpointer U d);
gboolean on_area_key_press (GtkWidget U * w, GdkEventKey * e, gpointer U data);
gboolean on_area_key_release (GtkWidget U * w, GdkEventKey U * e, gpointer U data);
