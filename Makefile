CC       = gcc
CFLAGS   = $$(pkg-config gtk+-2.0 --cflags) -Wall -Wextra -ansi -pedantic
LIBS     = $$(pkg-config gtk+-2.0 --libs) -Wall -Wextra -ansi -pedantic -lm
LDFLAGS  = -fsanitize=address -fsanitize=undefined -fsanitize=shift -fsanitize=integer-divide-by-zero -fsanitize=unreachable -fsanitize=vla-bound -fsanitize=null -fsanitize=return -fsanitize=signed-integer-overflow -fsanitize=bool -fsanitize=enum
RM       = rm -f

SRCDIR   = src
OBJDIR   = obj
BINDIR   = ./

SOURCES  = $(wildcard $(SRCDIR)/*.c)
INCLUDES = $(wildcard $(SRCDIR)/*.h)
OBJECTS  = $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
BINARY   = $(BINDIR)/ElementalBubble

all: $(BINARY)

$(BINARY): $(OBJECTS)
	@$(CC) $(LIBS) $(LDFLAGS) $^ -o $@
	@echo "Compiled" $@ "successfully."

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@mkdir -p $(OBJDIR)
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo "(CC) "$<""

.PHONEY: clean
clean:
	@$(RM) $(OBJECTS)
	@echo "Objects removed."

.PHONEY: remove
remove: clean
	@$(RM) $(BINDIR)/$(BINARY)
	@echo "Executable removed."
