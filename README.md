# Elemental Bubble

## Introduction

Elemental Bubble is a typical Bubble Shooter game done through the GTK+ library.


## Library
 - ISO C library
 - GTK2 & Cairo


## Code style
C ANSI.
Pedantic options enabled.
Code is intended with 4 spaces, no tabs.  
A line should not exceed 72 characters.  
It follows Allman indent style.
